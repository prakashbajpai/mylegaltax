<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mylegaltax' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'qAfc~/P}+S=w1H6PA4c/N;|[?&S<C$iu1/J1oF J3<i{&YErH)c7vW[k:2rrro!i' );
define( 'SECURE_AUTH_KEY',  'Tpcq*8(q37*,]_v4Zs!bpny8~[]`<WbvX6A(.!5kGi)+$IhPn}.z<KW~;@V&Ri)O' );
define( 'LOGGED_IN_KEY',    'Mb<}<[)=2#r}F)g+x)C|&xs8hkZ~[=(@I(zdt^)w6[8=X%DQ&fH6~5`#4,Dc1Jds' );
define( 'NONCE_KEY',        'Uocqhf7J~-#f7}OUOk&0+cl~ mF!bM8vHO@Q!Ld4wMh?D}!GhJU~GV-DLu;e#sn8' );
define( 'AUTH_SALT',        's(;Jfg`D_$W8v]jv/>=!E;0?cLff!@*,*kM!*Yis^;xztx)k2_vF4x~l-?H~W76$' );
define( 'SECURE_AUTH_SALT', 'x.3Qhv`gx}xt9f:9Uun9%r{$UhlcLI*Lm3RgJD;IV(fh_n=$_0/)4vSxR@A~G1(+' );
define( 'LOGGED_IN_SALT',   'H*0lg/624H>YqG6`GPGf &^iGpl>.R!.qBnnx>gj+:)hJvPw_U`F:&o&u mH=ESs' );
define( 'NONCE_SALT',       'g1Asvtg93MKl#H,cKRw0b=&Y7@VsNV<&f&7#2]3JB 9]5d4qmv^&W-Kt%3(>hZ}*' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'mlt_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
